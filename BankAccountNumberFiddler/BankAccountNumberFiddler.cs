﻿using System;
using System.Linq;
using System.Numerics;
using System.Text.RegularExpressions;

namespace BankAccountNumberFiddler
{
	/// <summary>
	/// Eemeli Manninen 2020-02-05
	/// </summary>
	class BankAccountNumberFiddler
	{
		static void Main(string[] args)
		{
			char key = '0';
			while (key != 'x')
			{
				Console.Write("\rEnter short format finnish BBAN number: ");
				string input = Console.ReadLine();

				FinnishBankAccountNumber number = new FinnishBankAccountNumber(input);
				if (number.IsValid)
				{
					Console.WriteLine($"BBAN { number.GetShortBBAN() } is { number.GetLongBBAN() } as long format BBAN and { number.GetIBAN() } as IBAN ");
				}
				else
				{
					Console.WriteLine("Input is not a valid BBAN number");
				}

				Console.WriteLine("Press x to exit, anything else to try again");
				key = Console.ReadKey().KeyChar;
			}
		}
	}

	public class FinnishBankAccountNumber
	{
		public FinnishBankAccountNumber(string inputShortBBAN)
		{
			// Check for input validity and set values for both BBAN and IBAN properties
			// This way the convert operation is always only done once
			IsValid = TryConvertBBANToIBAN(inputShortBBAN);

			// Could also throw an ArgumentException for invalid input BUT having available validity information instead allows the calling code to not have to try-catch wrap the constructor call to check the validity
		}
		private string CountryCode { get { return "FI";  } }
		public bool IsValid { get; private set; }
		private string ShortBBAN { get; set; }
		public string GetShortBBAN()
		{
			if (IsValid)
			{
				return ShortBBAN;
			}
			else
			{
				throw new InvalidOperationException();
			}
		}

		private string LongBBAN { get; set; }
		public string GetLongBBAN()
		{
			if (IsValid)
			{
				return LongBBAN;
			}
			else
			{
				throw new InvalidOperationException();
			}
		}

		private string IBAN { get; set; }
		public string GetIBAN()
		{
			if (IsValid)
			{
				return IBAN;
			}
			else
			{
				throw new InvalidOperationException();
			}
		}

		/// <summary>
		/// Check that the input is of finnish short BBAN format and convert it to long BBAN and IBAN with current country code property
		/// </summary>
		/// <param name="userInput"></param>
		/// <returns></returns>
		private bool TryConvertBBANToIBAN(string inputShortBBAN)
		{
			// Check input string format with regex - only numbers with exactly one dash in between are accepted (short format)
			// Shorter than minimum length cutoff strings are automatically invalid as well
			if (!Regex.Match(inputShortBBAN, @"^\d+-\d+$").Success || inputShortBBAN.Length < 8)
			{
				return false;
			}

			// This could in the future also allow long format BBAN numbers to be converted to IBAN format
			ShortBBAN = inputShortBBAN;

			// Trim off the dash
			string machineReadableNumber = inputShortBBAN.Replace("-", "");

			// Split the dash-free number depending on the leading digit
			int cutoffPoint;
			if (new char[] { '4', '5' }.Contains(machineReadableNumber[0]))
			{
				cutoffPoint = 7;
			}
			else
			{
				cutoffPoint = 6;
			}

			string begin = machineReadableNumber.Substring(0, cutoffPoint);
			string end = machineReadableNumber.Substring(begin.Length, machineReadableNumber.Length - begin.Length);

			// Fill the void with zeroes until total length of the string is 14
			int numberOfZerosToAdd = 14 - machineReadableNumber.Length;
			foreach (var i in Enumerable.Range(0, numberOfZerosToAdd))
			{
				begin += "0";
			}
			machineReadableNumber = begin + end;

			// Number up to this part is needed for the final number
			// This is also the long format BBAN
			LongBBAN = machineReadableNumber;

			// Add country identifier as a number
			// Convert letters to numbers such as that A = 10 etc and add to number as integers
			foreach (char c in CountryCode)
			{
				if (char.IsUpper(c))
				{
					machineReadableNumber += (c - 55).ToString();
				}
				// All current ISO country codes have only uppercase letters so this case is bad news
				else
				{
					throw new FormatException($"Country code { CountryCode } is invalid");
				}
			}

			// Add two zeros after the country identifier
			machineReadableNumber += "00";

			// Calculate check number - int and double are not big enough
			int checkNumber;

			ulong bigNumberForCalculation = 0ul;
			bool success = ulong.TryParse(machineReadableNumber, out bigNumberForCalculation);
			if (success)
			{
				ulong remainder = bigNumberForCalculation % 97ul;
				checkNumber = 98 - (int)remainder;
			}
			// ulong only carries to 18,446,744,073,709,551,615 - we're gonna need a bigger boat
			// This is slow so only use it if necessary
			else
			{
				BigInteger biggerNumberForCalculation = BigInteger.Parse(machineReadableNumber);
				BigInteger remainder = BigInteger.Zero;
				BigInteger.DivRem(biggerNumberForCalculation, BigInteger.Parse("97"), out remainder);

				checkNumber = 98 - int.Parse(remainder.ToString());
			}

			// Assemble the final IBAN
			string checkNumberString = checkNumber.ToString();
			if (checkNumber < 10)
			{
				checkNumberString = "0" + checkNumberString;
			}

			IBAN = CountryCode + checkNumberString + LongBBAN;

			return true;
		}
	}
}